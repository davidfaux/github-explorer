import "jest-preset-angular";
import "./jestGlobalMocks";

const WARN_SUPPRESSING_PATTERNS = [
  /Could not find Angular Material core theme/,
  /Could not find HammerJS/,
  /The "longpress" event cannot be bound because Hammer.JS is not loaded and no custom loader has been specified./,
  /The "slidestart" event cannot be bound because Hammer.JS is not loaded and no custom loader has been specified./,
  /The "slide" event cannot be bound because Hammer.JS is not loaded and no custom loader has been specified./,
  /The "slideend" event cannot be bound because Hammer.JS is not loaded and no custom loader has been specified./,
];

const warn = console.warn;

Object.defineProperty(console, "warn", {
  value: (...params: string[]) => {
    if (!WARN_SUPPRESSING_PATTERNS.some((pattern) => pattern.test(params[0]))) {
      warn(...params);
    }
  },
});

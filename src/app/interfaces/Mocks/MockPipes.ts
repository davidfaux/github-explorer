import { Pipe, PipeTransform } from "@angular/core";

@Pipe({ name: "DatePipe" })
export class MockDatePipe implements PipeTransform {
  transform(...args) {
    return "29.06.2018 15:12";
  }
}

@Pipe({ name: "textLimiter" })
export class MockTextLimiterPipe implements PipeTransform {
  transform(...args) {
    return "abc";
  }
}

@Pipe({ name: "numberAbbr" })
export class MockNumberAbbrPipe implements PipeTransform {
  transform(...args) {
    return "1k";
  }
}

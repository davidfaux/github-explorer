import { GitHubIssue } from "@app/interfaces";
import { MockUser } from "./MockUser";
export const MockIssue: GitHubIssue = {
  html_url: "https://github.com/test/testtest/issues/2",
  number: 2,
  title: "Test test",
  user: MockUser,
  state: "closed",
  comments: 0,
  watchers_count: 0,
  forks: 0,
  open_issues: 1,
  created_at: "2020-05-31T02:37:16Z",
  updated_at: "2020-09-21T14:47:19Z",
  closed_at: "2020-09-21T14:47:19Z",
};

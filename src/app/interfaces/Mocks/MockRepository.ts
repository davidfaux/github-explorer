import { GitHubRepository } from "@app/interfaces";
import { MockUser } from "./MockUser";
export const MockRepository: GitHubRepository = {
  id: 123,
  node_id: "abc123",
  name: "Test Repo",
  full_name: "Test Repo Full Name",
  private: false,
  owner: MockUser,
  stargazers_count: 2,
  watchers_count: 3,
  forks: 1,
  created_at: "2020-01-01 23:59:59",
  updated_at: "2020-01-01 23:59:59",
  description: "Test",
  html_url: "https://example.com",
  language: "TestScript",
  open_issues: 2,
  license: {
    name: "Free",
  },
};

import { GitHubRepository } from "./GitHubRepository";

// tslint:disable: variable-name
export class GitHubRepositoryResponse {
  total_count: number;
  incomplete_results: boolean;
  items: GitHubRepository[];
}

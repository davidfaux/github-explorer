import { GitHubIssue } from "@app/interfaces";

// tslint:disable: variable-name
export class GitHubIssueResponse {
  total_count: number;
  incomplete_results: boolean;
  items: GitHubIssue[];
}

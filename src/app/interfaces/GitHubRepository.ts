import { GitHubUser } from "./GitHubUser";

// tslint:disable: variable-name
export class GitHubRepository {
  id: number;
  node_id: string;
  name: string;
  full_name: string;
  private: boolean;
  owner: GitHubUser;
  html_url: string;
  description: string;
  language: string;
  stargazers_count: number;
  watchers_count: number;
  forks: number;
  open_issues: number;
  license: {
    name: string;
  };
  updated_at: string;
  created_at: string;
}

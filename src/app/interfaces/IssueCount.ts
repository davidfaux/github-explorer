export interface IssueCount {
  open: number;
  closed: number;
}

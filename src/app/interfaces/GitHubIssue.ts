import { GitHubUser } from "./GitHubUser";

// tslint:disable: variable-name
export class GitHubIssue {
  html_url: string;
  number: number;
  title: string;
  user: GitHubUser;
  created_at: string;
  updated_at: string;
  closed_at: string | null;
  comments: number;
  watchers_count: number;
  forks: number;
  open_issues: number;
  state: string;
}

import {
  GitHubIssue,
  GitHubIssueResponse,
  GitHubRepository,
  GitHubRepositoryResponse,
} from "@app/interfaces";

import { GitHubService } from "./git-hub.service";
import { HttpClientTestingModule } from "@angular/common/http/testing";
import { MatSnackBar } from "@angular/material";
import { MockIssue } from "@app/interfaces/Mocks/MockIssue";
import { MockRepository } from "@app/interfaces/Mocks/MockRepository";
import { TestBed } from "@angular/core/testing";
import { of } from "rxjs";

describe("GitHubService", () => {
  let httpClientSpy: { get: jasmine.Spy };
  let snackSpy: { get: jasmine.Spy };
  let gitHubService: GitHubService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [
        {
          provide: MatSnackBar,
          useValue: {
            open: () => {},
          },
        },
      ],
    });
    httpClientSpy = jasmine.createSpyObj("HttpClient", ["get"]);
    snackSpy = jasmine.createSpyObj("MatSnackBar", ["open"]);
    gitHubService = new GitHubService(httpClientSpy as any, snackSpy as any);
  });

  it("should be created", () => {
    const service: GitHubService = TestBed.get(GitHubService);
    expect(service).toBeTruthy();
  });

  it("should return repositories that match keyword", () => {
    const expectedRepos: GitHubRepository[] = [
      { id: 1, name: "Repo1" } as GitHubRepository,
      { id: 2, name: "Repo2" } as GitHubRepository,
    ];

    const expectedResponse: GitHubRepositoryResponse = {
      incomplete_results: false,
      total_count: 2,
      items: expectedRepos,
    };

    httpClientSpy.get.and.returnValue(of(expectedResponse));

    gitHubService
      .search("abc")
      .subscribe(
        (res) => expect(res).toEqual(expectedResponse, "expected response"),
        fail
      );
  });

  it("should return issues from provided repository", () => {
    const expectedIssues: GitHubIssue[] = [
      { title: "Issue1" } as GitHubIssue,
      { title: "Issue2" } as GitHubIssue,
    ];

    httpClientSpy.get.and.returnValue(of(expectedIssues));

    gitHubService
      .getIssues("test", "abc")
      .subscribe(
        (res) => expect(res).toEqual(expectedIssues, "expected issues"),
        fail
      );
  });

  it("should return issue count based on status from provided repository", () => {
    const expectedResponse: GitHubIssueResponse = {
      total_count: 1,
      incomplete_results: false,
      items: [{ ...MockIssue }],
    };
    httpClientSpy.get.and.returnValue(of(expectedResponse));

    gitHubService
      .getIssuesWithCounts("test", "abc", "open")
      .subscribe(
        (res) => expect(res).toEqual(expectedResponse, "expected response"),
        fail
      );
  });

  it("should get issue counts of opened and closed issues", () => {
    const openIssues = of({
      total_count: 1,
      incomplete_results: false,
      items: [{ ...MockIssue }],
    });
    const closedIssues = of({
      total_count: 2,
      incomplete_results: false,
      items: [{ ...MockIssue }],
    });
    const spy = spyOn(gitHubService, "getIssuesWithCounts").and.returnValues(
      openIssues,
      closedIssues
    );
    gitHubService.getIssueCounts("xyz", "qwerty").subscribe((res) => {
      expect(res).toEqual({ open: 1, closed: 2 });
    });
    expect(spy).toHaveBeenCalledTimes(2);
  });

  it("should get repository that match the provided owner and name", () => {
    const expectedRepo: GitHubRepository = { ...MockRepository };

    httpClientSpy.get.and.returnValue(of(expectedRepo));

    gitHubService
      .getRepository("test", "abc")
      .subscribe(
        (res) => expect(res).toEqual(expectedRepo, "expected repository"),
        fail
      );
  });
});

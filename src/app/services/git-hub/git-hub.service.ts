import {
  GitHubIssue,
  GitHubIssueResponse,
  GitHubRepository,
  GitHubRepositoryResponse,
} from "@app/interfaces";
import { catchError, map } from "rxjs/operators";
import { forkJoin, of } from "rxjs";

import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { MatSnackBar } from "@angular/material/snack-bar";
import { Observable } from "rxjs/internal/Observable";

@Injectable({
  providedIn: "root",
})
export class GitHubService {
  // Ordinarily API uris would be specified in the environmental
  // file so that we could inject a branch specific one during
  // build time for testing. This isn't required in this case as
  // we'll only be using GitHub's production uri
  public gitHubUri = "https://api.github.com";

  constructor(private http: HttpClient, private snackBar: MatSnackBar) {}

  public search(
    searchTerm: string,
    page = 1,
    perPage = 10
  ): Observable<GitHubRepositoryResponse> {
    return this.http
      .get<GitHubRepositoryResponse>(
        `${this.gitHubUri}/search/repositories?q=${searchTerm}&page=${page}&per_page=${perPage}`
      )
      .pipe(
        catchError(
          this.handleError(
            `An error occurred while retrieving repositories containing '${searchTerm}' `,
            {
              total_count: 0,
              incomplete_results: true,
              items: [],
            }
          )
        )
      );
  }

  public getIssues(
    owner: string,
    name: string,
    status: "open" | "closed" | "all" = "all",
    page = 1,
    perPage = 10
  ): Observable<GitHubIssue[]> {
    const state = status !== "all" ? `&state=${status}` : "";
    return this.http
      .get<GitHubIssue[]>(
        `${this.gitHubUri}/repos/${owner}/${name}/issues?page=${page}&per_page=${perPage}${state}`
      )
      .pipe(
        catchError(
          this.handleError(
            `Could not retrieve issues linked to ${owner}\\${name}`,
            []
          )
        )
      );
  }

  public getIssuesWithCounts(
    owner: string,
    name: string,
    status: "open" | "closed"
  ): Observable<GitHubIssueResponse> {
    return this.http.get<GitHubIssueResponse>(
      `${this.gitHubUri}/search/issues?q=repo:${owner}/${name}+type:issue+state:${status}&page=1&per_page=1`
    );
  }

  public getIssueCounts(
    owner: string,
    name: string
  ): Observable<{ open: number; closed: number }> {
    return forkJoin([
      this.getIssuesWithCounts(owner, name, "open"),
      this.getIssuesWithCounts(owner, name, "closed"),
    ]).pipe(
      map(([open, closed]) => {
        return { open: open.total_count, closed: closed.total_count };
      }),
      catchError(this.handleError(null, { open: 0, closed: 0 }))
    );
  }

  public getRepository(
    owner: string,
    name: string
  ): Observable<GitHubRepository> {
    return this.http
      .get<GitHubRepository>(`${this.gitHubUri}/repos/${owner}/${name}`)
      .pipe(
        catchError(
          this.handleError(
            `An error occurred while retrieving ${owner}\\${name}`,
            {} as GitHubRepository
          )
        )
      );
  }

  public handleError<T>(message = null, result?: T) {
    return (error: any): Observable<T> => {
      console.error(error);
      if (error.status === 403) {
        message =
          message + " Rate limit reached. Please try again in a little bit.";
      }
      if (message) {
        this.snackBar.open(message, "OK", {
          duration: 5000,
        });
      }
      return of(result as T);
    };
  }
}

import { AppRoutingModule } from "@app/app-routing.module";
import { CommonModule } from "@angular/common";
import { FlexLayoutModule } from "@angular/flex-layout";
import { LayoutComponent } from "./layout.component";
import { MatToolbarModule } from "@angular/material/toolbar";
import { NgModule } from "@angular/core";
import { PagesModule } from "@pages/pages.module";

@NgModule({
  declarations: [LayoutComponent],
  imports: [
    CommonModule,
    PagesModule,
    MatToolbarModule,
    AppRoutingModule,
    FlexLayoutModule,
  ],
  exports: [LayoutComponent],
})
export class LayoutModule {}

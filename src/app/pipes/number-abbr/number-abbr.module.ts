import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { NumberAbbrPipe } from "./number-abbr.pipe";

@NgModule({
  imports: [CommonModule],
  declarations: [NumberAbbrPipe],
  exports: [NumberAbbrPipe],
})
export class NumberAbbrPipeModule {}

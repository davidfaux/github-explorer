import { Pipe, PipeTransform } from "@angular/core";

@Pipe({
  name: "numberAbbr",
  pure: false,
})
export class NumberAbbrPipe implements PipeTransform {
  private abbreviations = ["k", "m", "b", "t"];
  private value = "";

  transform(input: any, decimalPlaces: number): string {
    if (input < 1000) {
      return input.toString();
    }

    decimalPlaces = Math.pow(10, decimalPlaces);
    for (let i = this.abbreviations.length - 1; i >= 0; i--) {
      const size = Math.pow(10, (i + 1) * 3);
      if (size <= input) {
        input = Math.round((input * decimalPlaces) / size) / decimalPlaces;
        if (input === 1000 && i < this.abbreviations.length - 1) {
          input = 1;
          i++;
        }
        this.value = input.toString() + this.abbreviations[i];
        break;
      }
    }
    return this.value;
  }
}

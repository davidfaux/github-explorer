import { NumberAbbrPipe } from "./number-abbr.pipe";

describe("NumberAbbrPipe", () => {
  it("should create an instance", () => {
    const pipe = new NumberAbbrPipe();
    expect(pipe).toBeTruthy();
  });

  it("should return the abbreviated number", () => {
    const pipe = new NumberAbbrPipe();
    const value = "1.2k";
    const output = pipe.transform(1200, 1);
    expect(output).toEqual(value);
  });

  it("should return the number if less than 1000", () => {
    const pipe = new NumberAbbrPipe();
    const value = "500";
    const output = pipe.transform(500, 1);
    expect(output).toEqual(value);
  });
});

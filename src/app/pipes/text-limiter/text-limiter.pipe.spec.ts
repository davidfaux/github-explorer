import { TextLimiterPipe } from "./text-limiter.pipe";

describe("TextLimiterPipe", () => {
  it("should create an instance", () => {
    const pipe = new TextLimiterPipe();
    expect(pipe).toBeTruthy();
  });

  it("should not alter text when string is shorter than limit", () => {
    const pipe = new TextLimiterPipe();
    const str = "This is a string";
    const output = pipe.transform(str, 100);
    expect(output).toEqual(str);
  });

  it("should shorten string to limit when exceeds limit", () => {
    const pipe = new TextLimiterPipe();
    const str = "This is a string";
    const output = pipe.transform(str, 5);
    expect(output).toEqual("This…");
  });

  it("should trim shortened string", () => {
    const pipe = new TextLimiterPipe();
    const str = "This is a string";
    const output = pipe.transform(str, 6);
    expect(output).toEqual("This…");
  });

  it("should return the input string if no limit", () => {
    const pipe = new TextLimiterPipe();
    const str = "This is a string";
    const output = pipe.transform(str, null);
    expect(output).toEqual(str);
  });
});

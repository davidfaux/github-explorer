import { Pipe, PipeTransform } from "@angular/core";

@Pipe({
  name: "textLimiter",
  pure: false,
})
export class TextLimiterPipe implements PipeTransform {
  transform(value: string, limit: number): any {
    if (typeof value !== "string") {
      return value;
    }

    if (!limit) {
      return value;
    }

    return value.length > limit
      ? value.substr(0, limit - 1).trim() + "…"
      : value;
  }
}

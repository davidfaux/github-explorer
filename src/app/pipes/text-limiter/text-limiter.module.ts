import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { TextLimiterPipe } from "./text-limiter.pipe";

@NgModule({
  imports: [CommonModule],
  declarations: [TextLimiterPipe],
  exports: [TextLimiterPipe],
})
export class TextLimiterPipeModule {}

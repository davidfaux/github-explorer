import * as moment from "moment";

import { Pipe, PipeTransform } from "@angular/core";

@Pipe({
  name: "DatePipe",
  pure: true,
})
export class DatePipe implements PipeTransform {
  transform(
    date: string,
    fromNow = false,
    format: string = "YYYY/MM/DD HH:mm"
  ): any {
    if (fromNow) {
      return moment(date).fromNow();
    }

    return moment(date).format(format);
  }
}

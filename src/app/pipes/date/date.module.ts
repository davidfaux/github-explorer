import { CommonModule } from "@angular/common";
import { DatePipe } from "./date.pipe";
import { NgModule } from "@angular/core";

@NgModule({
  imports: [CommonModule],
  declarations: [DatePipe],
  exports: [DatePipe],
})
export class DatePipeModule {}

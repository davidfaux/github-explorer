import * as moment from "moment";

import { DatePipe } from "./date.pipe";

describe("DatePipe", () => {
  it("should create an instance", () => {
    const pipe = new DatePipe();
    expect(pipe).toBeTruthy();
  });

  it("should return the date format with hours and minutes", () => {
    const date = "2019-05-01T00:00:00.000Z";

    const formatDate = moment(date).format("YYYY/MM/DD");

    const pipe = new DatePipe();

    expect(pipe.transform(date, false, "YYYY/MM/DD")).toEqual(formatDate);
  });

  it("should return the default date", () => {
    const date = "2019-05-01T00:00:00.000Z";

    const formatDate = moment(date).format("YYYY/MM/DD HH:mm");

    const pipe = new DatePipe();

    expect(pipe.transform(date)).toEqual(formatDate);
  });

  it("should return the fromNow date", () => {
    const pastDate = moment().subtract(1, "day").toISOString();

    const pipe = new DatePipe();

    expect(pipe.transform(pastDate, true)).toEqual("a day ago");
  });
});

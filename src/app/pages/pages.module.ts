import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { PageNotFoundModule } from "@pages/page-not-found/page-not-found.module";
import { SearchModule } from "@pages/search/search.module";
import { ViewRepositoryModule } from "@pages/view-repository/view-repository.module";

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    ViewRepositoryModule,
    PageNotFoundModule,
    SearchModule,
  ],
})
export class PagesModule {}

import { Component, OnDestroy, OnInit, Inject } from "@angular/core";

import { ActivatedRoute } from "@angular/router";
import { GitHubService } from "@app/services/git-hub/git-hub.service";
import { IssueCount } from "@app/interfaces/IssueCount";
import { takeUntil } from "rxjs/operators";
import { PageScrollService } from "ngx-page-scroll-core";
import { DOCUMENT } from "@angular/common";
import { PageEvent } from "@angular/material/paginator";
import { Subject } from "rxjs/internal/Subject";
import { GitHubRepository, GitHubIssue } from "@app/interfaces";

@Component({
  selector: "app-view-repository",
  templateUrl: "./view-repository.component.html",
  styleUrls: ["./view-repository.component.scss"],
})
export class ViewRepositoryComponent implements OnInit, OnDestroy {
  private destroy$: Subject<void> = new Subject<void>();
  public repo: GitHubRepository;
  public owner: string;
  public name: string;
  public issueState: "open" | "closed" | "all" = "open";
  public issues: GitHubIssue[];
  public pageNumber = 1;
  public pageSize = 5;
  public totalResults: number;
  public searching = false;
  public repoNotFound = false;
  public issueCounts: IssueCount;

  constructor(
    private route: ActivatedRoute,
    private service: GitHubService,
    private pageScrollService: PageScrollService,
    @Inject(DOCUMENT) private document: any
  ) {}

  ngOnInit() {
    this.setNameAndOwner();
    this.getRepository();
    this.getIssues();
  }

  ngOnDestroy() {
    this.destroy$.next();
    this.destroy$.complete();
  }

  public setNameAndOwner() {
    const map = this.route.snapshot.paramMap;
    this.owner = map.get("owner");
    this.name = map.get("name");
  }

  public getRepository() {
    this.service
      .getRepository(this.owner, this.name)
      .pipe(takeUntil(this.destroy$))
      .subscribe((repo) => {
        if (repo.id) {
          this.repo = repo;
        } else {
          this.repoNotFound = true;
        }
      });
  }

  public handleStateChange() {
    this.issues = null;
    this.getIssues();
    this.setTotalPages();
  }

  public setTotalPages() {
    switch (this.issueState) {
      case "all":
        this.totalResults = this.issueCounts.closed + this.issueCounts.open;
        break;

      case "closed":
        this.totalResults = this.issueCounts.closed;
        break;

      default:
        this.totalResults = this.issueCounts.open;
        break;
    }
  }

  public setIssueCounts(issueCounts: IssueCount) {
    this.issueCounts = issueCounts;
    this.setTotalPages();
  }

  public handlePage(event: PageEvent): void {
    this.pageScrollService.scroll({
      document: this.document,
      scrollTarget: "#toolbar",
    });
    this.pageSize = event.pageSize;
    this.pageNumber =
      event.pageSize !== this.pageSize ? 1 : event.pageIndex + 1;
    this.getIssues();
    this.setTotalPages();
  }

  public getIssues() {
    this.searching = true;
    this.issues = null;
    this.service
      .getIssues(
        this.owner,
        this.name,
        this.issueState,
        this.pageNumber,
        this.pageSize
      )
      .pipe(takeUntil(this.destroy$))
      .subscribe((issues) => {
        this.issues = issues;
        this.searching = false;
      });
  }
}

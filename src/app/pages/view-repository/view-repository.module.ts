import { CommonModule } from "@angular/common";
import { FlexLayoutModule } from "@angular/flex-layout";
import { FormsModule } from "@angular/forms";
import { IssueResultModule } from "@app/components/issue-result/issue-result.module";
import { MatButtonToggleModule } from "@angular/material/button-toggle";
import { MatIconModule } from "@angular/material/icon";
import { MatPaginatorModule } from "@angular/material/paginator";
import { MatProgressSpinnerModule } from "@angular/material/progress-spinner";
import { MetaChipModule } from "@app/components/meta-chip/meta-chip.module";
import { NgModule } from "@angular/core";
import { PieChartModule } from "@app/components/pie-chart/pie-chart.module";
import { RouterModule } from "@angular/router";
import { TextLimiterPipeModule } from "@app/pipes/text-limiter/text-limiter.module";
import { ViewRepositoryComponent } from "./view-repository.component";

@NgModule({
  declarations: [ViewRepositoryComponent],
  imports: [
    CommonModule,
    MetaChipModule,
    FlexLayoutModule,
    MatIconModule,
    PieChartModule,
    MatButtonToggleModule,
    FormsModule,
    MatProgressSpinnerModule,
    IssueResultModule,
    MatPaginatorModule,
    TextLimiterPipeModule,
    RouterModule,
  ],
})
export class ViewRepositoryModule {}

import { ComponentFixture, TestBed, async } from "@angular/core/testing";

import { ActivatedRoute } from "@angular/router";
import { CUSTOM_ELEMENTS_SCHEMA } from "@angular/core";
import { GitHubService } from "@app/services/git-hub/git-hub.service";
import { MockIssue } from "@app/interfaces/Mocks/MockIssue";
import { MockRepository } from "@app/interfaces/Mocks/MockRepository";
import { PageEvent } from "@angular/material";
import { PageScrollService } from "ngx-page-scroll-core";
import { RouterTestingModule } from "@angular/router/testing";
import { TextLimiterPipeModule } from "@app/pipes/text-limiter/text-limiter.module";
import { ViewRepositoryComponent } from "./view-repository.component";
import { of } from "rxjs/internal/observable/of";

describe("ViewRepositoryComponent", () => {
  let component: ViewRepositoryComponent;
  let fixture: ComponentFixture<ViewRepositoryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
      imports: [TextLimiterPipeModule, RouterTestingModule],
      providers: [
        {
          provide: GitHubService,
          useValue: {
            getRepository: () => of({ ...MockRepository }),
            getIssues: () => of([{ ...MockIssue }]),
          },
        },
        {
          provide: PageScrollService,
          useValue: {
            scroll: () => {},
          },
        },
        {
          provide: ActivatedRoute,
          useValue: {
            snapshot: {
              paramMap: new Map([
                ["owner", "testOwner"],
                ["name", "testName"],
              ]),
            },
          },
        },
      ],
      declarations: [ViewRepositoryComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewRepositoryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it("should create", () => {
    expect(component).toBeTruthy();
  });

  it("should set name and owner", () => {
    const activatedRoute = TestBed.get(ActivatedRoute);
    activatedRoute.snapshot.paramMap.set("owner", "xyz");
    activatedRoute.snapshot.paramMap.set("name", "abc");
    component.setNameAndOwner();
    expect(component.owner).toEqual("xyz");
    expect(component.name).toEqual("abc");
  });

  it("should get repository", () => {
    const gitHubService = TestBed.get(GitHubService);
    const spy = spyOn(gitHubService, "getRepository").and.returnValue(
      of({ ...MockRepository })
    );
    component.getRepository();
    expect(spy).toHaveBeenCalledWith("testOwner", "testName");
    expect(component.repo).toEqual({ ...MockRepository });
  });

  it("should get issues", () => {
    const gitHubService = TestBed.get(GitHubService);
    const spy = spyOn(gitHubService, "getIssues").and.returnValue(
      of([{ ...MockIssue }])
    );
    component.getIssues();

    expect(spy).toHaveBeenCalledWith("testOwner", "testName", "open", 1, 5);
    expect(component.issues).toEqual([{ ...MockIssue }]);
  });

  it("should handle state change", () => {
    const issuesSpy = spyOn(component, "getIssues");
    const pagesSpy = spyOn(component, "setTotalPages");
    component.handleStateChange();
    expect(issuesSpy).toHaveBeenCalled();
    expect(pagesSpy).toHaveBeenCalled();
    expect(component.issues).toBeNull();
  });

  it("should set total pages", () => {
    component.issueCounts = {
      open: 2,
      closed: 3,
    };

    component.issueState = "all";
    component.setTotalPages();
    expect(component.totalResults).toEqual(5);

    component.issueState = "closed";
    component.setTotalPages();
    expect(component.totalResults).toEqual(3);

    component.issueState = "open";
    component.setTotalPages();
    expect(component.totalResults).toEqual(2);
  });

  it("should set issue counts and then set total pages", () => {
    const spy = spyOn(component, "setTotalPages");
    component.setIssueCounts({ open: 2, closed: 3 });
    expect(spy).toHaveBeenCalled();
  });

  it("should handle the page event", () => {
    const event: PageEvent = {
      pageSize: 5,
      pageIndex: 2,
      length: 100,
    };

    const issuesSpy = spyOn(component, "getIssues");
    const pagesSpy = spyOn(component, "setTotalPages");
    component.handlePage(event);

    expect(component.pageSize).toEqual(5);
    expect(component.pageNumber).toEqual(3);
    expect(issuesSpy).toHaveBeenCalled();
    expect(pagesSpy).toHaveBeenCalled();
  });
});

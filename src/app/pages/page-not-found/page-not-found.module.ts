import { CommonModule } from "@angular/common";
import { FlexLayoutModule } from "@angular/flex-layout";
import { NgModule } from "@angular/core";
import { PageNotFoundComponent } from "./page-not-found.component";
import { RouterModule } from "@angular/router";

@NgModule({
  declarations: [PageNotFoundComponent],
  imports: [CommonModule, FlexLayoutModule, RouterModule],
})
export class PageNotFoundModule {}

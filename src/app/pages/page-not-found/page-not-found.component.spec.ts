import { ComponentFixture, TestBed, async } from "@angular/core/testing";

import { By } from "@angular/platform-browser";
import { CUSTOM_ELEMENTS_SCHEMA } from "@angular/core";
import { PageNotFoundComponent } from "./page-not-found.component";
import { Router } from "@angular/router";
import { RouterTestingModule } from "@angular/router/testing";

describe("PageNotFoundComponent", () => {
  let component: PageNotFoundComponent;
  let fixture: ComponentFixture<PageNotFoundComponent>;
  let router: Router;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
      declarations: [PageNotFoundComponent],
      imports: [RouterTestingModule.withRoutes([])],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PageNotFoundComponent);
    component = fixture.componentInstance;
    router = TestBed.get(Router);
    fixture.detectChanges();
  });

  it("should create", () => {
    expect(component).toBeTruthy();
  });

  it("should route home when `search for repository` link is clicked", () => {
    const href = fixture.debugElement
      .query(By.css(".home-link"))
      .nativeElement.getAttribute("href");
    expect(href).toEqual("/");
  });
});

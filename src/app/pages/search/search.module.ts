import { CommonModule } from "@angular/common";
import { FlexLayoutModule } from "@angular/flex-layout";
import { FormsModule } from "@angular/forms";
import { MatButtonModule } from "@angular/material/button";
import { MatPaginatorModule } from "@angular/material/paginator";
import { MatProgressSpinnerModule } from "@angular/material/progress-spinner";
import { NgModule } from "@angular/core";
import { NgxPageScrollCoreModule } from "ngx-page-scroll-core";
import { SearchComponent } from "./search.component";
import { SearchResultModule } from "@app/components/search-result/search-result.module";
import { TextLimiterPipeModule } from "@app/pipes/text-limiter/text-limiter.module";

@NgModule({
  declarations: [SearchComponent],
  imports: [
    CommonModule,
    FlexLayoutModule,
    FormsModule,
    MatPaginatorModule,
    TextLimiterPipeModule,
    MatButtonModule,
    SearchResultModule,
    MatProgressSpinnerModule,
    NgxPageScrollCoreModule.forRoot({
      duration: 500,
    }),
  ],
})
export class SearchModule {}

import {
  ComponentFixture,
  TestBed,
  async,
  fakeAsync,
} from "@angular/core/testing";

import { CUSTOM_ELEMENTS_SCHEMA } from "@angular/core";
import { FormsModule } from "@angular/forms";
import { GitHubService } from "@app/services/git-hub/git-hub.service";
import { MockRepository } from "@app/interfaces/Mocks/MockRepository";
import { PageEvent } from "@angular/material/paginator";
import { PageScrollService } from "ngx-page-scroll-core";
import { SearchComponent } from "./search.component";
import { of } from "rxjs/internal/observable/of";

describe("SearchComponent", () => {
  let component: SearchComponent;
  let fixture: ComponentFixture<SearchComponent>;
  let githubService: GitHubService;
  let pageScrollService: PageScrollService;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
      imports: [FormsModule],
      providers: [
        {
          provide: GitHubService,
          useValue: {
            search: () =>
              of({
                total_count: 1,
                items: [{ ...MockRepository }],
              }),
          },
        },
        {
          provide: PageScrollService,
          useValue: {
            scroll: () => {},
          },
        },
      ],
      declarations: [SearchComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SearchComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    githubService = TestBed.get(GitHubService);
    pageScrollService = TestBed.get(PageScrollService);
  });

  it("should create", () => {
    expect(component).toBeTruthy();
  });

  it("should emit new search term when changed", () => {
    const spy = spyOn(component.searchTermChanged$, "next");
    component.searchTerm = "abc";
    component.searchChanged();
    expect(spy).toHaveBeenCalledWith("abc");
  });

  it("should call clearSearch when the search term is an empty string", () => {
    const spy = spyOn(component, "clearSearch");
    component.searchTerm = "";
    component.searchChanged();
    expect(spy).toHaveBeenCalled();
  });

  it("should store the searchState to localStorage", () => {
    const spy = spyOn(localStorage, "setItem");
    const state = JSON.stringify({
      searchTerm: "xyz",
      pageSize: component.pageSize,
      pageNumber: component.pageNumber,
    });
    component.searchTerm = "xyz";
    component.saveState();
    expect(spy).toHaveBeenCalledWith("searchState", state);
  });

  it("should perform a search", fakeAsync(() => {
    const repos = [{ ...MockRepository }];
    const spy = spyOn(githubService, "search").and.returnValue(
      of({
        total_count: 1,
        incomplete_results: false,
        items: repos,
      })
    );
    component.searchTerm = "test";
    component.pageNumber = 1;
    component.pageSize = 5;

    component.performSearch();
    expect(spy).toHaveBeenCalledWith("test", 1, 5);
    expect(component.totalResults).toEqual(1);
    expect(component.repos).toEqual(repos);
  }));

  it("should clear the search", () => {
    component.searchTerm = "test";
    component.repos = [{ ...MockRepository }];
    component.pageNumber = 100;
    const spy = spyOn(localStorage, "removeItem");
    component.clearSearch();
    expect(component.searchTerm).toBeNull();
    expect(component.repos).toEqual([]);
    expect(component.pageNumber).toEqual(1);
    expect(spy).toHaveBeenCalledWith("searchState");
  });

  it("should handle the page event", () => {
    const event: PageEvent = {
      pageSize: 5,
      pageIndex: 2,
      length: 100,
    };
    component.searchTerm = "test page event";
    const spy = spyOn(component, "searchChanged");
    component.handlePage(event);

    expect(component.pageSize).toEqual(5);
    expect(component.pageNumber).toEqual(3);
    expect(spy).toHaveBeenCalled();
  });
});

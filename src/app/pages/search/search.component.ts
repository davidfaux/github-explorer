import { Component, OnDestroy, OnInit, Inject } from "@angular/core";
import { debounceTime, takeUntil, distinctUntilChanged } from "rxjs/operators";

import { GitHubRepository } from "@app/interfaces";
import { GitHubService } from "@app/services/git-hub/git-hub.service";
import { PageEvent } from "@angular/material/paginator";
import { Subject } from "rxjs/internal/Subject";
import { PageScrollService } from "ngx-page-scroll-core";
import { DOCUMENT } from "@angular/common";

@Component({
  selector: "app-search",
  templateUrl: "./search.component.html",
  styleUrls: ["./search.component.scss"],
})
export class SearchComponent implements OnInit, OnDestroy {
  private destroy$: Subject<void> = new Subject<void>();
  private storageKey = "searchState";

  public searchTermChanged$: Subject<string> = new Subject<string>();
  public searchTerm: string = null;
  public repos: GitHubRepository[] = [];
  public totalResults: number;
  public pageNumber = 1;
  public pageSize = 5;
  public searching = false;

  constructor(
    private service: GitHubService,
    private pageScrollService: PageScrollService,
    @Inject(DOCUMENT) private document: any
  ) {}

  ngOnInit() {
    const searchState = JSON.parse(localStorage.getItem(this.storageKey));
    if (searchState) {
      this.searchTerm = searchState.searchTerm;
      this.pageNumber = searchState.pageNumber;
      this.pageSize = searchState.pageSize;
      this.performSearch();
    }
    this.searchTermChanged$
      .pipe(debounceTime(500), takeUntil(this.destroy$))
      .subscribe(() => this.performSearch());
  }

  ngOnDestroy() {
    this.destroy$.next();
    this.destroy$.complete();
  }

  public searchChanged(): void {
    if (!!this.searchTerm.trim()) {
      this.searching = true;
      this.searchTermChanged$.next(this.searchTerm.trim());
      this.saveState();
    } else {
      this.clearSearch();
    }
  }

  public performSearch() {
    if (!!this.searchTerm) {
      this.repos = [];
      this.service
        .search(this.searchTerm, this.pageNumber, this.pageSize)
        .pipe(takeUntil(this.destroy$), distinctUntilChanged())
        .subscribe(
          (res) => {
            this.totalResults = res.total_count;
            this.repos = res.items;
            this.searching = false;
          },
          () => {
            this.searching = false;
          }
        );
    }
  }

  public clearSearch() {
    this.searchTerm = null;
    this.repos = [];
    this.pageNumber = 1;
    localStorage.removeItem(this.storageKey);
  }

  public saveState() {
    const searchState = {
      searchTerm: this.searchTerm,
      pageSize: this.pageSize,
      pageNumber: this.pageNumber,
    };
    localStorage.setItem(this.storageKey, JSON.stringify(searchState));
  }

  public handlePage(event: PageEvent): void {
    this.pageScrollService.scroll({
      document: this.document,
      scrollTarget: "#toolbar",
    });
    this.pageSize = event.pageSize;
    this.pageNumber =
      event.pageSize !== this.pageSize ? 1 : event.pageIndex + 1;
    this.searchChanged();
  }
}

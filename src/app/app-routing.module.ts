import { RouterModule, Routes } from "@angular/router";

import { NgModule } from "@angular/core";
import { PageNotFoundComponent } from "@pages/page-not-found/page-not-found.component";
import { SearchComponent } from "@pages/search/search.component";
import { ViewRepositoryComponent } from "@pages/view-repository/view-repository.component";

const routes: Routes = [
  {
    path: "",
    redirectTo: "/search",
    pathMatch: "full",
  },
  {
    path: "repo/:owner/:name",
    component: ViewRepositoryComponent,
  },
  {
    path: "search",
    component: SearchComponent,
  },
  { path: "404", component: PageNotFoundComponent },
  { path: "**", component: PageNotFoundComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}

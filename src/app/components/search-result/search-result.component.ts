import { Component, Input } from "@angular/core";

import { GitHubRepository } from "@app/interfaces";
import { Router } from "@angular/router";

@Component({
  selector: "app-search-result",
  templateUrl: "./search-result.component.html",
  styleUrls: ["./search-result.component.scss"],
})
export class SearchResultComponent {
  @Input() result: GitHubRepository;

  constructor(private router: Router) {}

  public navigate(repo: string): void {
    this.router.navigate([`/repo/${repo}`]);
  }
}

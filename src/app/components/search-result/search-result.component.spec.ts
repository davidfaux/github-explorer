import { ComponentFixture, TestBed, async } from "@angular/core/testing";

import { CUSTOM_ELEMENTS_SCHEMA } from "@angular/core";
import { GitHubRepository } from "@app/interfaces";
import { MockDatePipe } from "@app/interfaces/Mocks/MockPipes";
import { MockRepository } from "@app/interfaces/Mocks/MockRepository";
import { Router } from "@angular/router";
import { SearchResultComponent } from "./search-result.component";

describe("SearchResultComponent", () => {
  let component: SearchResultComponent;
  let fixture: ComponentFixture<SearchResultComponent>;
  let router: Router;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
      providers: [
        {
          provide: Router,
          useValue: {
            navigate: () => {},
          },
        },
      ],
      declarations: [SearchResultComponent, MockDatePipe],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SearchResultComponent);
    component = fixture.componentInstance;
    component.result = { ...MockRepository } as GitHubRepository;
    fixture.detectChanges();
    router = TestBed.get(Router);
  });

  it("should create", () => {
    expect(component).toBeTruthy();
  });

  it("should navigate to repository", () => {
    const spy = spyOn(router, "navigate");
    component.navigate("abc/123");
    expect(spy).toHaveBeenCalledWith(["/repo/abc/123"]);
  });
});

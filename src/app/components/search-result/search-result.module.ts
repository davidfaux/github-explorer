import { CommonModule } from "@angular/common";
import { DatePipeModule } from "@app/pipes/date/date.module";
import { FlexLayoutModule } from "@angular/flex-layout";
import { MatIconModule } from "@angular/material/icon";
import { MatTooltipModule } from "@angular/material/tooltip";
import { NgModule } from "@angular/core";
import { SearchResultComponent } from "./search-result.component";

@NgModule({
  declarations: [SearchResultComponent],
  imports: [
    CommonModule,
    MatIconModule,
    FlexLayoutModule,
    DatePipeModule,
    MatTooltipModule,
  ],
  exports: [SearchResultComponent],
})
export class SearchResultModule {}

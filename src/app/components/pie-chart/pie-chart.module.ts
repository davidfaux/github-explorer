import { CommonModule } from "@angular/common";
import { FlexLayoutModule } from "@angular/flex-layout";
import { NgModule } from "@angular/core";
import { PieChartComponent } from "./pie-chart.component";

@NgModule({
  declarations: [PieChartComponent],
  imports: [CommonModule, FlexLayoutModule],
  exports: [PieChartComponent],
})
export class PieChartModule {}

import { ComponentFixture, TestBed, async } from "@angular/core/testing";

import { CUSTOM_ELEMENTS_SCHEMA } from "@angular/core";
import { GitHubService } from "@app/services/git-hub/git-hub.service";
import { PieChartComponent } from "./pie-chart.component";
import { of } from "rxjs";

describe("PieChartComponent", () => {
  let component: PieChartComponent;
  let fixture: ComponentFixture<PieChartComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
      providers: [
        {
          provide: GitHubService,
          useValue: {
            getIssueCounts: () => {
              return of({ open: 1, closed: 1 });
            },
          },
        },
      ],
      declarations: [PieChartComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PieChartComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it("should create", () => {
    expect(component).toBeTruthy();
  });
});

import * as d3 from "d3";

import { Component, EventEmitter, Input, OnInit, Output } from "@angular/core";

import { GitHubService } from "@app/services/git-hub/git-hub.service";
import { IssueCount } from "@app/interfaces";

@Component({
  selector: "app-pie-chart",
  templateUrl: "./pie-chart.component.html",
  styleUrls: ["./pie-chart.component.scss"],
})
export class PieChartComponent implements OnInit {
  @Input() owner: string;
  @Input() name: string;
  @Output() issueCounts: EventEmitter<IssueCount> = new EventEmitter();

  public openCount: number;
  public closedCount: number;
  public openPercent: number;
  public legend = {
    closed: {
      label: "Closed",
      color: "#3f51b5",
    },
    open: {
      label: "Open",
      color: "#ff4081",
    },
  };

  constructor(private service: GitHubService) {}

  ngOnInit() {
    this.service
      .getIssueCounts(this.owner, this.name)
      .subscribe(({ open, closed }) => {
        this.openCount = open;
        this.closedCount = closed;
        this.openPercent = (open / (open + closed)) * 100;
        this.issueCounts.emit({ open, closed });
      });
  }
}

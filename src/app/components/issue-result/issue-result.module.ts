import { CommonModule } from "@angular/common";
import { DatePipeModule } from "@app/pipes/date/date.module";
import { FlexLayoutModule } from "@angular/flex-layout";
import { IssueResultComponent } from "./issue-result.component";
import { MatIconModule } from "@angular/material/icon";
import { NgModule } from "@angular/core";
import { TextLimiterPipeModule } from "@app/pipes/text-limiter/text-limiter.module";

@NgModule({
  declarations: [IssueResultComponent],
  imports: [
    CommonModule,
    FlexLayoutModule,
    MatIconModule,
    DatePipeModule,
    TextLimiterPipeModule,
  ],
  exports: [IssueResultComponent],
})
export class IssueResultModule {}

import { Component, Input } from "@angular/core";

import { GitHubIssue } from "@app/interfaces";

@Component({
  selector: "app-issue-result",
  templateUrl: "./issue-result.component.html",
  styleUrls: ["./issue-result.component.scss"],
})
export class IssueResultComponent {
  @Input() issue: GitHubIssue;
  public commentMapping: { [k: string]: string } = {
    "=0": "No comments",
    "=1": "1 comment",
    other: "# comments",
  };
}

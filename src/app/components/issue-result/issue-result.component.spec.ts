import { ComponentFixture, TestBed, async } from "@angular/core/testing";
import {
  MockDatePipe,
  MockTextLimiterPipe,
} from "@app/interfaces/Mocks/MockPipes";

import { CUSTOM_ELEMENTS_SCHEMA } from "@angular/core";
import { GitHubIssue } from "@app/interfaces";
import { IssueResultComponent } from "./issue-result.component";
import { MockIssue } from "@app/interfaces/Mocks/MockIssue";

describe("IssueResultComponent", () => {
  let component: IssueResultComponent;
  let fixture: ComponentFixture<IssueResultComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
      declarations: [IssueResultComponent, MockDatePipe, MockTextLimiterPipe],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IssueResultComponent);
    component = fixture.componentInstance;
    component.issue = { ...MockIssue } as GitHubIssue;
    fixture.detectChanges();
  });

  it("should create", () => {
    expect(component).toBeTruthy();
  });
});

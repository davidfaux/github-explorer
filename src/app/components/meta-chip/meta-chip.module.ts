import { CommonModule } from "@angular/common";
import { FlexLayoutModule } from "@angular/flex-layout";
import { MatIconModule } from "@angular/material/icon";
import { MetaChipComponent } from "./meta-chip.component";
import { NgModule } from "@angular/core";
import { NumberAbbrPipeModule } from "@app/pipes/number-abbr/number-abbr.module";

@NgModule({
  declarations: [MetaChipComponent],
  imports: [
    CommonModule,
    MatIconModule,
    FlexLayoutModule,
    NumberAbbrPipeModule,
  ],
  exports: [MetaChipComponent],
})
export class MetaChipModule {}

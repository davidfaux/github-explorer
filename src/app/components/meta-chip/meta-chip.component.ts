import { Component, Input } from "@angular/core";

@Component({
  selector: "app-meta-chip",
  templateUrl: "./meta-chip.component.html",
  styleUrls: ["./meta-chip.component.scss"],
})
export class MetaChipComponent {
  @Input() icon: string;
  @Input() label: string;
  @Input() value: string;
}

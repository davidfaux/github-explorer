import { ComponentFixture, TestBed, async } from "@angular/core/testing";

import { CUSTOM_ELEMENTS_SCHEMA } from "@angular/core";
import { MetaChipComponent } from "./meta-chip.component";
import { MockNumberAbbrPipe } from "@app/interfaces/Mocks/MockPipes";

describe("MetaChipComponent", () => {
  let component: MetaChipComponent;
  let fixture: ComponentFixture<MetaChipComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
      declarations: [MetaChipComponent, MockNumberAbbrPipe],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MetaChipComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it("should create", () => {
    expect(component).toBeTruthy();
  });
});

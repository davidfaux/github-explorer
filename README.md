# GitHubDemo

## Table of contents


  - [Overview](#overview)
  - [Installation](#installation)
  - [Design Choices](#design-choices)
    - [Modules](#modules)
    - [Defined paths](#defined-paths)
    - [RxJS operators](#rxjs-operators)
    - [Search state persistance](#search-state-persistance)
    - [Barrel files](#barrel-files)
    - [CSS normalization](#css-normalization)
    - [Flex Layout](#flex-layout)
    - [Use of rem units](#use-of-rem-units)
    - [SCSS variables](#scss-variables)
    - [SVG Pie chart](#svg-pie-chart)
    - [Karma and Jasmine](#karma-and-jasmine)
  - [Potential Enhancements:](#potential-enhancements)

---

## Overview

My development environment has Angular CLI v8.3.29 which is the requirement for my current projects and as a result when using `ng new ProjectName` it creates a project using Angular 8.2.14. I've used the built in schematics within Angular CLI extensively as this lends itself to a more cohesive project.

I've opted for Angular Material (v8.2.3) for components for its frictionless integration into Angular, comprehensive documentation, reliable support and low cognitive load as it is something that I use daily. I have used one of the built in Material themes for this project.

My development style is to keep an eye on specifications and try not to over-engineer the product. 


---

## Installation

Install the required packages from NPM

```
npm install
```

Once installation has completed you can start the project. Note: This will open [http://localhost:4200](http://localhost:4200) in a new tab in your default browser 

```
npm run start
```

To build the project

```
npm run build // Without optimization, ideal for testing
npm run build:prod // With optimization, ideal for production release
```

To run tests
```
npm run test
```

---

## Design Choices

### Modules

All components have modules for ease of importing and constrain scope to where required. Page components are all exported by a single module to keep the App module lean and clean.

### Defined paths

I've added defined paths within `tsconfig.json` to clean up imports and aid the developer's mental map of the application's layout.

### RxJS operators

All RxJS observables are unsubscribed from once the component is destroyed to prevent memory leaks. I have made use of `debounceTime` to minimize GitHub rate limiting. In the [enhancements](#potential-enhancements) section I suggest that a future enhancement would prevent this from being an issue.

### Search state persistance

I've decided to use localStorage to persist search state which allows a user to return to their search result set. The downside is that this is persisted until the localStorage is cleared which means should a user returns to the app after a long absence their last search results will once again be retrieved. I initially considered using cookies but that would mean installing another package which seemed extraneous. Another approach could be to add a date field to the state and clear storage if this date is exceeded but this smelled of over engineering so I ditched the idea.

### Barrel files

I've made use of barrel file to clean up imports and aid with readability. The only area where this made sense to use was the export of the various interfaces.

### CSS normalization

I made use of [Normalize.css](https://necolas.github.io/normalize.css/) for element consistency. I use this for most projects as it retains some useful browser defaults and addresses some common bugs.

### Flex Layout

I've use Angular Flex Layout (v~8.0.0-beta.27) throughout the application to apply FlexBox CSS to the UI. Its responsive engine helped me make this application mobile friendly(-ish)

### Use of rem units

I have used Root ems through out the application which keeps sizes relative to a root value which gives it a harmonious design and an easy update should add an enhancement effecting font size.

### SCSS variables

Font colour choices have been extracted to a variables file that gets imported into component SCSS files. If a future enhancement requires a change in font colours you would only need to update one file. This does not necessarily need to be [limited to colours](https://sass-lang.com/documentation/variables).

### SVG Pie chart

I created a simple SVG to meet the pie chart specification. It fulfils the requirement however if more intricate graphing functionality is needed I would rather make use of a graphing library such as D3 or Plotly.

### Karma and Jasmine

I left the default testing framework in place and have written tests accordingly. If the project starts growing I would consider swopping to Jest for a faster and more dev friendly experience.

## Potential Enhancements:

**Complete interfaces** - Current interfaces are a subset of what GitHub returns. I've only defined fields that I was using in the application.

**Ordering** - The specification didn't mention ordering so I have left it at GitHub's default. A future enhancement could add the option of ordering the results by last updated, fork numbers or stars.

**Lazy Loading** - The routing in this application is very basic and if future enhancements require more routes be added to the application it would be a good idea to configure lazy loading which only loads modules when required and reduces the size of the initial bundle in turn decreasing load times.

**Error logging** - The service has a dedicated error handler and therefore could easily integrate into external error logging mechanisms.

**Github Token** - Integrate into GitHub as a OAuth or GitHub app to prevent rate limiting
